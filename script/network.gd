extends Node

var Messages := preload("res://script/messages.gd").new()

signal account_created
signal login
signal battle_invite
signal battle_invite_send
signal battle_start
signal battle_result_received
signal monster_info_received
signal starter_received
signal team_received
signal velocity_update_received
signal world_update_received
signal dialog_update_received
signal object_removed

var _peer: PacketPeerHermod
#var _websocket

const LOGIN_REQ: String = "0001"
const LOGIN_RESP = "0002"
const CREATE_ACCOUNT_REQ = "0003"
const CREATE_ACCOUNT_RESP = "0004"

const SEND_MONSTER_INTO_BATTLE = "b003"
const SEND_ATTACK_MONSTER = "b004"
const SEND_BATTLE_REQ = "b005"
const SEND_BATTLE_RESP = "b006"
const RECEIVE_BATTLE_INVITE = "b007"
const ACCEPT_BATTLE_INVITE = "b008"
const BATTLE_START = "b009"
const BATTLE_RESULT_RESP = "b00a"

var isReady = true

func _process(delta):
	if !_peer:
		return
	var b := _peer.get_packet()
	if b.size() > 0:
		_handle_received_data(b)

	_peer.resend_lost_packages()
	_peer.keep_alive()


func connect_to_host(host_ip: String = "127.0.0.1", host_port: int = 8081, protocol_id = 0xF001):
	var _hermod = PacketPeerHermod.new()
	_hermod.protocolID = protocol_id
	var error = _hermod.connect_to_host(host_ip, host_port)
	if error:
		return error

	_peer = _hermod


func login(breederID):
	var m = Messages.createLogin(breederID)

	var error = Messages.validateLogin(m)
	if error:
		print(error)
		return

	var msg = _create_message(Messages.Type_Login, 0, m)
	_send_data(msg)


func create_account(name, species):
	var m = Messages.createCreateBreeder(name, species)

	var error = Messages.validateCreateBreeder(m)
	if error:
		print(error)
		return

	var msg = _create_message(Messages.Type_CreateBreeder, 0, m)
	_send_data(msg)


func send_challenge(breederID):
	var m = Messages.createBattleRequest(breederID)

	var error = Messages.validateBattleRequest(m)
	if error:
		print(error)
		return

	var msg = _create_message(Messages.Type_BattleRequest, 0, m)
	_send_data(msg)


func accept_challenge(challengeID):
	var m = Messages.createBattleRequestAccept(challengeID)

	var error = Messages.validateBattleRequestAccept(m)
	if error:
		print(error)
		return

	var msg = _create_message(Messages.Type_BattleRequestAccept, 0, m)

	_send_data(msg)


func send_monster_into_battle(breederID, teamIndex, side, position):
	var m = Messages.createBattleSendMonster(
		breederID, Messages.createBattleLocation(position, side), teamIndex
	)

	var error = Messages.validateBattleSendMonster(m)
	if error:
		print(error)
		return

	var msg = _create_message(Messages.Type_BattleSendMonster, 0, m)
	_send_data(msg)


func send_attack(breederID, moveIndex, _location, _targetLocations):
	var m = Messages.createBattleAttack(
		Messages.createBattleLocation(_location.position, _location.side),
		breederID,
		moveIndex,
		[Messages.createBattleLocation(_targetLocations[0].position, _targetLocations[0].side)]
	)

	var error = Messages.validateBattleAttack(m)
	if error:
		print(error)
		return

	var msg = _create_message(Messages.Type_BattleAttack, 0, m)
	_send_data(msg)


func request_monster_data(monsterIds):
	var m = Messages.createRequestMonsterInfo(monsterIds)
	var error = Messages.validateRequestMonsterInfo(m)
	if error:
		printerr(error)
		return

	var msgID = OS.get_unix_time()

	var msg = _create_message(Messages.Type_RequestMonsterInfo, msgID, m)
	_send_data(msg)
	return msgID


func request_starter():
	_send_data(_create_message(Messages.Type_RequestStarter, 0, Messages.createRequestStarter()))


func request_wild_battle(area):
	_send_data(
		_create_message(Messages.Type_RequestWildBattle, 0, Messages.createRequestWildBattle(area))
	)


func send_capture(breederId, actionFor, target, item):
	_send_data(
		_create_message(
			Messages.Type_BattleCapture,
			0,
			Messages.createBattleCapture(actionFor, breederId, item, target)
		)
	)


func request_team(breederID):
	_send_data(_create_message(Messages.Type_RequestTeam, 0, Messages.createRequestTeam(breederID)))


func surrender_battle(breederID, location):
	_send_data(
		_create_message(
			Messages.Type_BattleSurrender, 0, Messages.createBattleSurrender(breederID, location)
		)
	)


func send_input(breederID, direction, interact):
	_send_data(
		_create_message(
			Messages.Type_OverworldInput,
			0,
			Messages.createOverworldInput(breederID, {"x": direction.x, "y": direction.y}, interact)
		)
	)


func _create_message(msgType, id, data):
	return {
		"Type": msgType,
		"ID": id,
		"Data": data,
	}


func _send_data(data):
	var json = JSON.print(data)
	_peer.put_packet(json.to_utf8())


func _handle_received_data(array: PoolByteArray):
	#var array = _peer.get_packet()
	var result = JSON.parse(array.get_string_from_utf8()).result

	match result.type:
		Messages.Type_CreateBreederResponse:
			emit_signal("account_created", result.data)
		Messages.Type_LoginResponse:
			emit_signal("login", result.data)
		Messages.Type_BattleRequestResponse:
			emit_signal("battle_invite_send")
		Messages.Type_BattleRequestInvite:
			emit_signal("battle_invite", result.data)
		Messages.Type_BattleCreation:
			emit_signal("battle_start", result.data)
		Messages.Type_BattleResult:
			emit_signal("battle_result_received", result.data)
		Messages.Type_RequestMonsterInfoResponse:
			emit_signal("monster_info_received", result.data, result.responseID)
		Messages.Type_RequestStarterResponse:
			emit_signal("starter_received", result.data)
		Messages.Type_RequestTeamResponse:
			emit_signal("team_received", result.data)
		Messages.Type_OverworldUpdatePositions:
			emit_signal("velocity_update_received", result.data)
		Messages.Type_OverworldUpdateWorldPositions:
			emit_signal("world_update_received", result.data)
		Messages.Type_OverworldUpdateDialogs:
			emit_signal("dialog_update_received", result.data)
		Messages.Type_OverworldRemove:
			emit_signal("object_removed", result.data)
		_:
			printerr("Unknown: " + JSON.print(result))


func _on_conn_established(_protocol):
	isReady = true
