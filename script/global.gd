extends Node

var breederID: int
var worldObjID: int

var battleScene = preload("res://scenes/battle/BattleField.tscn")
var worldScene = preload("res://scenes/overworld/World.tscn").instance()

var precScene

func _ready():
	network.connect("login", self, "_on_login")

	network.connect("battle_start", self, "_on_battleStart")

func _on_login(data):
	breederID = data.breeder.id
	worldObjID = data.worldId

func changeToWorld():
	for ch in get_tree().root.get_children():
		if ch.name == "network" or ch.name == "global":
			continue
		ch.queue_free()
	get_tree().root.add_child(worldScene)

func _on_battleStart(data):
	var scene = battleScene.instance()
	scene.battleInfo = data
	
	
	var chview = get_tree().root.get_node_or_null("ChallengeView")
	if chview:
		precScene = chview
	
	var world = get_tree().root.get_node_or_null("World")
	if world:
		
		precScene = world
		
	get_tree().root.remove_child(precScene)

		
	get_tree().root.add_child(scene)
	
	scene.connect("battle_finished", self, "_on_battleFinished")
	
func _on_battleFinished():
	for ch in get_tree().root.get_children():
		if ch.name == "network" or ch.name == "global":
			continue
		ch.queue_free()
	get_tree().root.add_child(precScene)
#	get_tree().change_scene("res://scenes/battle/Challenge.tscn")
