const Type_BattleAttack: String = "BattleAttack"


func validateBattleAttack(data) -> String:
	var err: String

	err = validateBattleLocation(data["Attacker"])
	if err:
		return "Attribute 'Attacker' failed a nested validation check: " + err

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	if data["MoveIndex"] < 0:
		return "Attribute 'MoveIndex' failed the validation check: positiveInt"

	for w in data["Targets"]:
		var v = w

		err = validateBattleLocation(v)
		if err:
			return "Attribute 'Targets' failed a nested validation check: " + err

	return ""


func createBattleAttack(Attacker: Dictionary, BreederID: int, MoveIndex: int, Targets: Array):
	return {
		"Attacker": Attacker, "BreederID": BreederID, "MoveIndex": MoveIndex, "Targets": Targets
	}


const Type_BattleCapture: String = "BattleCapture"


func validateBattleCapture(data) -> String:
	var err: String

	err = validateBattleLocation(data["ActionFor"])
	if err:
		return "Attribute 'ActionFor' failed a nested validation check: " + err

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	err = validateBattleLocation(data["Target"])
	if err:
		return "Attribute 'Target' failed a nested validation check: " + err

	return ""


func createBattleCapture(ActionFor: Dictionary, BreederID: int, Item: String, Target: Dictionary):
	return {"ActionFor": ActionFor, "BreederID": BreederID, "Item": Item, "Target": Target}


const Type_BattleCreation: String = "BattleCreation"


func validateBattleCreation(data) -> String:
	var err: String

	if data["BattleID"] < 0:
		return "Attribute 'BattleID' failed the validation check: positiveInt"

	for w in data["Locations"]:
		for v in w:
			err = validateBattleLocation(v)
			if err:
				return "Attribute 'Locations' failed a nested validation check: " + err

	for w in data["Teams"]:
		for v in w:
			err = validateMonsterView(v)
			if err:
				return "Attribute 'Teams' failed a nested validation check: " + err

	return ""


func createBattleCreation(BattleID: int, Locations: Dictionary, Teams: Dictionary):
	return {"BattleID": BattleID, "Locations": Locations, "Teams": Teams}


const Type_BattleRequest: String = "BattleRequest"


func validateBattleRequest(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	return ""


func createBattleRequest(BreederID: int):
	return {"BreederID": BreederID}


const Type_BattleRequestAccept: String = "BattleRequestAccept"


func validateBattleRequestAccept(data) -> String:
	var err: String

	if data["RequestID"] < 0:
		return "Attribute 'RequestID' failed the validation check: positiveInt"

	return ""


func createBattleRequestAccept(RequestID: int):
	return {"RequestID": RequestID}


const Type_BattleRequestCancel: String = "BattleRequestCancel"


func validateBattleRequestCancel(data) -> String:
	var err: String

	if data["RequestID"] < 0:
		return "Attribute 'RequestID' failed the validation check: positiveInt"

	return ""


func createBattleRequestCancel(RequestID: int):
	return {"RequestID": RequestID}


const Type_BattleRequestCancelResponse: String = "BattleRequestCancelResponse"


func validateBattleRequestCancelResponse(data) -> String:
	var err: String

	if data["RequestID"] < 0:
		return "Attribute 'RequestID' failed the validation check: positiveInt"

	return ""


func createBattleRequestCancelResponse(RequestID: int):
	return {"RequestID": RequestID}


const Type_BattleRequestInvite: String = "BattleRequestInvite"


func validateBattleRequestInvite(data) -> String:
	var err: String

	if data["From"] < 0:
		return "Attribute 'From' failed the validation check: positiveInt"

	if data["RequestID"] < 0:
		return "Attribute 'RequestID' failed the validation check: positiveInt"

	if data["To"] < 0:
		return "Attribute 'To' failed the validation check: positiveInt"

	return ""


func createBattleRequestInvite(From: int, RequestID: int, To: int):
	return {"From": From, "RequestID": RequestID, "To": To}


const Type_BattleRequestResponse: String = "BattleRequestResponse"


func validateBattleRequestResponse(data) -> String:
	var err: String

	return ""


func createBattleRequestResponse():
	return {}


const Type_BattleResult: String = "BattleResult"


func validateBattleResult(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	if data["Id"] < 0:
		return "Attribute 'Id' failed the validation check: positiveInt"

	return ""


func createBattleResult(BreederID: int, Id: int, Result, ResultType: String):
	return {"BreederID": BreederID, "Id": Id, "Result": Result, "ResultType": ResultType}


const Type_BattleSendMonster: String = "BattleSendMonster"


func validateBattleSendMonster(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	err = validateBattleLocation(data["Location"])
	if err:
		return "Attribute 'Location' failed a nested validation check: " + err

	if data["TeamIndex"] < 0:
		return "Attribute 'TeamIndex' failed the validation check: positiveInt"

	return ""


func createBattleSendMonster(BreederID: int, Location: Dictionary, TeamIndex: int):
	return {"BreederID": BreederID, "Location": Location, "TeamIndex": TeamIndex}


const Type_BattleSurrender: String = "BattleSurrender"


func validateBattleSurrender(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	err = validateBattleLocation(data["Location"])
	if err:
		return "Attribute 'Location' failed a nested validation check: " + err

	return ""


func createBattleSurrender(BreederID: int, Location: Dictionary):
	return {"BreederID": BreederID, "Location": Location}


func validateBreederView(data) -> String:
	var err: String

	if data["Id"] < 0:
		return "Attribute 'Id' failed the validation check: positiveInt"

	return ""


func createBreederView(Id: int, Name: String):
	return {"Id": Id, "Name": Name}


const Type_CreateBreeder: String = "CreateBreeder"


func validateCreateBreeder(data) -> String:
	var err: String

	return ""


func createCreateBreeder(Name: String, Species: String):
	return {"Name": Name, "Species": Species}


const Type_CreateBreederResponse: String = "CreateBreederResponse"


func validateCreateBreederResponse(data) -> String:
	var err: String

	err = validateBreederView(data["Breeder"])
	if err:
		return "Attribute 'Breeder' failed a nested validation check: " + err

	return ""


func createCreateBreederResponse(Breeder: Dictionary):
	return {"Breeder": Breeder}


func validateDetailedMonsterInfo(data) -> String:
	var err: String

	if data["Id"] < 0:
		return "Attribute 'Id' failed the validation check: positiveInt"

	if data["Level"] < 0:
		return "Attribute 'Level' failed the validation check: positiveInt"

	return ""


func createDetailedMonsterInfo(
	CurrentHP,
	CurrentStamina,
	Id: int,
	Level: int,
	Moves: Array,
	Name: String,
	Species: String,
	Status: Array
):
	return {
		"CurrentHP": CurrentHP,
		"CurrentStamina": CurrentStamina,
		"Id": Id,
		"Level": Level,
		"Moves": Moves,
		"Name": Name,
		"Species": Species,
		"Status": Status
	}


const Type_Error: String = "Error"


func validateError(data) -> String:
	var err: String

	return ""


func createError():
	return {}


func validateBattleLocation(data) -> String:
	var err: String

	if data["Position"] < 0:
		return "Attribute 'Position' failed the validation check: positiveInt"

	if data["Side"] < 0:
		return "Attribute 'Side' failed the validation check: positiveInt"

	return ""


func createBattleLocation(Position: int, Side: int):
	return {"Position": Position, "Side": Side}


const Type_Login: String = "Login"


func validateLogin(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	return ""


func createLogin(BreederID: int):
	return {"BreederID": BreederID}


const Type_LoginResponse: String = "LoginResponse"


func validateLoginResponse(data) -> String:
	var err: String

	err = validateBreederView(data["Breeder"])
	if err:
		return "Attribute 'Breeder' failed a nested validation check: " + err

	return ""


func createLoginResponse(Breeder: Dictionary, WorldId: int):
	return {"Breeder": Breeder, "WorldId": WorldId}


func validateMonsterView(data) -> String:
	var err: String

	if data["Id"] < 0:
		return "Attribute 'Id' failed the validation check: positiveInt"

	if data["Level"] < 0:
		return "Attribute 'Level' failed the validation check: positiveInt"

	return ""


func createMonsterView(
	CurrentHP: float,
	CurrentStamina: float,
	Id: int,
	Level: int,
	Name: String,
	Species: String,
	Status: Array
):
	return {
		"CurrentHP": CurrentHP,
		"CurrentStamina": CurrentStamina,
		"Id": Id,
		"Level": Level,
		"Name": Name,
		"Species": Species,
		"Status": Status
	}


const Type_OverworldInput: String = "OverworldInput"


func validateOverworldInput(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	err = validateVector2D(data["Direction"])
	if err:
		return "Attribute 'Direction' failed a nested validation check: " + err

	return ""


func createOverworldInput(BreederID: int, Direction: Dictionary, Interaction: bool):
	return {"BreederID": BreederID, "Direction": Direction, "Interaction": Interaction}


const Type_OverworldMove: String = "OverworldMove"


func validateOverworldMove(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	err = validateVector2D(data["Direction"])
	if err:
		return "Attribute 'Direction' failed a nested validation check: " + err

	return ""


func createOverworldMove(BreederID: int, Direction: Dictionary):
	return {"BreederID": BreederID, "Direction": Direction}


func validateOverworldPositionUpdate(data) -> String:
	var err: String

	if data["BreederId"] < 0:
		return "Attribute 'BreederId' failed the validation check: positiveInt"

	err = validateVector2D(data["Position"])
	if err:
		return "Attribute 'Position' failed a nested validation check: " + err

	err = validateVector2D(data["Velocity"])
	if err:
		return "Attribute 'Velocity' failed a nested validation check: " + err

	return ""


func createOverworldPositionUpdate(BreederId: int, Position: Dictionary, Velocity: Dictionary):
	return {"BreederId": BreederId, "Position": Position, "Velocity": Velocity}


const Type_OverworldRemove: String = "OverworldRemove"


func validateOverworldRemove(data) -> String:
	var err: String

	return ""


func createOverworldRemove(WorldID: int):
	return {"WorldID": WorldID}


const Type_OverworldUpdateDialogs: String = "OverworldUpdateDialogs"


func validateOverworldUpdateDialogs(data) -> String:
	var err: String

	return ""


func createOverworldUpdateDialogs(Dialog: String):
	return {"Dialog": Dialog}


const Type_OverworldUpdatePositions: String = "OverworldUpdatePositions"


func validateOverworldUpdatePositions(data) -> String:
	var err: String

	for w in data["Updates"]:
		var v = w

		err = validateOverworldPositionUpdate(v)
		if err:
			return "Attribute 'Updates' failed a nested validation check: " + err

	return ""


func createOverworldUpdatePositions(Updates: Array):
	return {"Updates": Updates}


const Type_OverworldUpdateWorldPositions: String = "OverworldUpdateWorldPositions"


func validateOverworldUpdateWorldPositions(data) -> String:
	var err: String

	for w in data["Updates"]:
		var v = w

		err = validateOverworldWorldUpdate(v)
		if err:
			return "Attribute 'Updates' failed a nested validation check: " + err

	return ""


func createOverworldUpdateWorldPositions(Updates: Array):
	return {"Updates": Updates}


func validateOverworldWorldUpdate(data) -> String:
	var err: String

	err = validateVector2D(data["Area"])
	if err:
		return "Attribute 'Area' failed a nested validation check: " + err

	err = validateVector2D(data["Offset"])
	if err:
		return "Attribute 'Offset' failed a nested validation check: " + err

	err = validateVector2D(data["Size"])
	if err:
		return "Attribute 'Size' failed a nested validation check: " + err

	for w in data["Tiles"]:
		var v = w

		if v < 0:
			return "Attribute 'Tiles' failed the validation check: positiveInt"

	return ""


func createOverworldWorldUpdate(
	Area: Dictionary, Offset: Dictionary, Size: Dictionary, Tiles: Array, World: String
):
	return {"Area": Area, "Offset": Offset, "Size": Size, "Tiles": Tiles, "World": World}


const Type_RequestMonsterInfo: String = "RequestMonsterInfo"


func validateRequestMonsterInfo(data) -> String:
	var err: String

	for w in data["MonsterIDs"]:
		var v = w

		if v < 0:
			return "Attribute 'MonsterIDs' failed the validation check: positiveInt"

	return ""


func createRequestMonsterInfo(MonsterIDs: Array):
	return {"MonsterIDs": MonsterIDs}


const Type_RequestMonsterInfoResponse: String = "RequestMonsterInfoResponse"


func validateRequestMonsterInfoResponse(data) -> String:
	var err: String

	for w in data["MonsterInfo"]:
		var v = w

		err = validateDetailedMonsterInfo(v)
		if err:
			return "Attribute 'MonsterInfo' failed a nested validation check: " + err

	return ""


func createRequestMonsterInfoResponse(MonsterInfo: Array):
	return {"MonsterInfo": MonsterInfo}


const Type_RequestStarter: String = "RequestStarter"


func validateRequestStarter(data) -> String:
	var err: String

	return ""


func createRequestStarter():
	return {}


const Type_RequestStarterResponse: String = "RequestStarterResponse"


func validateRequestStarterResponse(data) -> String:
	var err: String

	return ""


func createRequestStarterResponse(Species: Array):
	return {"Species": Species}


const Type_RequestTeam: String = "RequestTeam"


func validateRequestTeam(data) -> String:
	var err: String

	if data["BreederID"] < 0:
		return "Attribute 'BreederID' failed the validation check: positiveInt"

	return ""


func createRequestTeam(BreederID: int):
	return {"BreederID": BreederID}


const Type_RequestWildBattle: String = "RequestWildBattle"


func validateRequestWildBattle(data) -> String:
	var err: String

	return ""


func createRequestWildBattle(Area: String):
	return {"Area": Area}


const Type_RequestTeamResponse: String = "RequestTeamResponse"


func validateRequestTeamResponse(data) -> String:
	var err: String

	return ""


func createRequestTeamResponse():
	return {}


func validateVector2D(data) -> String:
	var err: String

	return ""


func createVector2D(X: float, Y: float):
	return {"X": X, "Y": Y}
