extends PacketPeerUDP

# Hermod is a reliable UDP implementation
#
# # How to use
# func _init():
# 	var hermod := PacketPeerHermod.new()
# 	hermod.protocolID = 0xF001
# 	hermod.connect_to_host("127.0.0.1", 8080)
#
# func _process(delta):
# 	var bytes := hermod.get_packet()
# 	process(bytes)
# 	hermod.resend_lost_packages()
# 	hermod.keep_alive()

class_name PacketPeerHermod

# The protocolID used by the server.
# Must be between 0 and 4294967295.
# Will be clamped to this range.
var protocolID: int setget set_protocolID

# The maximal time between messages.
# If this is exceeded, an empty message is send to the server to keep the connection alive.
var keep_alive_timeout = 500

# How long to wait for an ACK message.
# If this timeout is exceeded, the old message is assumed as lost and will be resend.
var resend_timeout = 1000

var _local_seq := 0
var _waiting_for_ack := {}

var _remote_seq := 0
var _needToAck := []

var _last_message := 0

const _max_uint32 := 4294967295


func set_protocolID(id):
	if id < 0:
		id = 0
	elif id > _max_uint32:
		id = _max_uint32
	protocolID = id


func get_packet() -> PoolByteArray:
	var b := .get_packet()
	if b.size() < 16:
		return PoolByteArray()
		
	var p := _retrieve_package(b)
	
# warning-ignore:return_value_discarded
	_waiting_for_ack.erase(p.ack)
	for ack in p.restore_acks():
# warning-ignore:return_value_discarded
		_waiting_for_ack.erase(ack)
		
	if _remote_seq < p.sequence:
		_remote_seq = p.sequence
	
	_needToAck.append(p.sequence)
	if _needToAck.size() > 32:
		_needToAck.pop_front()
		
	
	return p.data
	
	
func put_packet(b: PoolByteArray):
	var p := _Package.new()
	p.data = b
	
	p.protocolID = protocolID
	p.ack = _remote_seq
	
	for i in _needToAck:
		var z: int = p.ack - i
		p.prevAcks |= 1 << z
	
	_send(p)
	
	
func _send(p: _Package):
	p.sequence = _next_sequence()
# warning-ignore:return_value_discarded
	.put_packet(p.bytes())
	
	p.send = OS.get_system_time_msecs()
	_last_message = p.send
	_waiting_for_ack[p.sequence] = p


func resend_lost_packages():
	var packages := []
	for t in _waiting_for_ack:
		var p: _Package = _waiting_for_ack[t]
		var time := OS.get_system_time_msecs() 
		if time - p.send >= resend_timeout:
			packages.append(p)
	
	for p in packages:
# warning-ignore:return_value_discarded
		_waiting_for_ack.erase(p.sequence)
		_send(p)


func keep_alive():
	# If no package was send, send an empty message to keep the connection alive
	if OS.get_system_time_msecs() - _last_message >= keep_alive_timeout:
		put_packet(PoolByteArray())


func _next_sequence():
	_local_seq += 1
	return _local_seq


class Helper:
	static func _uint32_from_little_endian(b: PoolByteArray) -> int:

		var result := 0
		for i in range(4):
			result += b[i] << 8 * i
		return result
		
		
	static func _uint32_to_little_endian(n: int) -> PoolByteArray:
		var result := []
		for i in range(4):
			var m := n >> 8 * i
			
			# Only the last 8 bits are relevant
			result.append(m & 0b11111111)
	
		return PoolByteArray(result)
	
	
class _Package:
	var protocolID: int
	var sequence: int
	var ack: int
	var prevAcks: int
	var data: PoolByteArray
	
	var send: int
	
	
	func bytes() -> PoolByteArray:
		var result := PoolByteArray()
		result.append_array(Helper._uint32_to_little_endian(protocolID))
		result.append_array(Helper._uint32_to_little_endian(sequence))
		result.append_array(Helper._uint32_to_little_endian(ack))
		result.append_array(Helper._uint32_to_little_endian(prevAcks))
		result.append_array(data)
		
		return result
		
		
	func restore_acks() -> Array:
		var acks := Array()
		for i in range(32):
			if (prevAcks>>i)&1 == 1:
				acks.append(ack - i - 1)
		return acks

	
func _retrieve_package(b: PoolByteArray) -> _Package:
	var p := _Package.new()

	p.protocolID = Helper._uint32_from_little_endian(b.subarray(0,3))
	p.sequence = Helper._uint32_from_little_endian(b.subarray(4,7))
	p.ack = Helper._uint32_from_little_endian(b.subarray(8,11))
	p.prevAcks = Helper._uint32_from_little_endian(b.subarray(12,15))
	
	if b.size() > 16:
		p.data = b.subarray(16, b.size() - 1)

	return p

