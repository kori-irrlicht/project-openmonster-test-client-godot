extends Control

var _invite;

func _ready():
	network.connect("battle_invite", self, "_on_battle_invite")

func _on_AcceptButton_pressed():
	network.accept_challenge(_invite.requestID)

func _on_battle_invite(data):
	_invite = data;
	$"HBoxContainer/Challenger".text = str(data.from);
