extends PanelContainer

onready var ipField = $GridContainer/IP
onready var portField = $GridContainer/Port

func _on_Connect_pressed():
	var ip = ipField.text
	if !ip:
		ip = "127.0.0.1"
	var port = int(portField.text)
	var err = network.connect_to_host(ip, port)
	if !err:
		get_tree().change_scene("res://gui/login.tscn")
