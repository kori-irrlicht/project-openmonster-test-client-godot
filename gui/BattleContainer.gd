extends VBoxContainer

var teamScene = preload("res://gui/team/team.tscn")

var _teams;
var _side;

func _ready():
	network.connect("battle_start", self, "_on_battle_start")
	network.connect("battle_result_received", self, "_on_battle_result_received")
	network.connect("monster_info_received", self, "_on_monster_info_received")
	
func _on_monster_info_received(data):
	print(data)
	for team in _teams:
		for mon in _teams[team]:
			for m in data.monsterInfo:
				if mon.id == m.id:
					mon.moves = m.moves
	

func _on_battle_start(data):
	print(data)
	_teams = data.teams
	for i in data.teams:
		var team = data.teams[i]
		var inst = teamScene.instance()
		var breederID = int(i)
		var side = data.locations["%d" % breederID][0].side
		inst.set_team(team, breederID, side)
		$Teams.add_child(inst)
		$Teams.add_child(HSeparator.new())

func _on_battle_result_received(data):
	var result = data.result
	match data.resultType:
		"sendMonster":
			var team = _teams[str(data.breeder)]
			var monster = team[result.teamIndex]
			if data.breeder == global.breederID:
				$BattleField.create_attack_buttons(monster.moves)
			if result.spawnLocation.side == 0:
				$BattleField.set_monster_left(monster)
			else:
				$BattleField.set_monster_right(monster)
		"attack":
			for change in result.changes:
				var location = change.location
				var monster = $BattleField.get_monster_at(location)
				match change.change:
					"health":
						var new_hp = change.value
						monster.currentHP = new_hp
					"status":
						var new_status = change.value
						monster.status = new_status
					"stamina":
						var new_stamina = change.value
						monster.currentStamina = new_stamina
					_:
						print(change)
			$BattleField.update()
		"finished":
			var text = $Log/Label.text + '\n'
			if result.isDraw:
				text += "Draw"
			else:
				text += "Winner is team: " + str(result.winnerIndex)
				
			$Log/Label.text = text
		_:
			print(data)
	pass

func _on_BattleField_attack_pressed(attack_index):
	var side = global.breederID % 2
	network.send_attack(global.breederID, attack_index, {"side": side, "position": 0}, [{"side": 1-side, "position":0}])
