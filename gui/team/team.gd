extends HBoxContainer

var monsterStatScene = preload("res://gui/team/MonsterStat.tscn")

signal send_monster_into_battle

var side
var team

var positionToTeamIndex = {}


func set_team(team, breederID, side):
	var index = 0
	self.side = side
	self.team = team

	var ids = []

	for monster in team:
		var inst = monsterStatScene.instance()
		inst.set_monster(monster)

		var hbox = VBoxContainer.new()
		hbox.add_child(inst)

		if breederID == global.breederID:
			var button = Button.new()
			button.connect("pressed", self, "_send_monster", [index])
			button.text = "Send into battle"
			hbox.add_child(button)

			ids.push_back(monster.id)

		$".".add_child(hbox)
		$".".add_child(VSeparator.new())
		index = index + 1

	if ids.size() > 0:
		network.request_monster_data(ids)


func _send_monster(index):
	positionToTeamIndex[0] = index
	network.send_monster_into_battle(global.breederID, index, side, 0)
