extends VBoxContainer

var monster

func set_monster(monster):
	self.monster = monster
	$Name/NameVal.text = monster.name
	$Level/LevelVal.text = str(monster.level)
	$Status/StatusVal.text = to_json(monster.status)
	$HP/CurrentHP.text = str(monster.currentHP)
	$Stamina/CurrentStamina.text = str(monster.currentStamina)

func update():
	set_monster(monster)
