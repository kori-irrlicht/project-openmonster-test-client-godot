extends HBoxContainer

signal attack_pressed

func _ready():
	$Left.visible = false
	$Right.visible = false

func set_monster_left(monster):
	$Left/MonsterStat.set_monster(monster)
	$Left.visible = true
	
func set_monster_right(monster):
	$Right/MonsterStat.set_monster(monster)
	$Right.visible = true

func update():
	$Left/MonsterStat.update()
	$Right/MonsterStat.update()

func get_monster_at(location):
	var monster
	if location.side == 0:
		monster = $Left/MonsterStat.monster
	else:
		monster = $Right/MonsterStat.monster
	return monster

func _on_AttackButton_pressed(attack_index):
	emit_signal("attack_pressed", attack_index)

func create_attack_buttons(attacks):
	for ch in $Attacks.get_children():
		$Attacks.remove_child(ch)
	
	for i in range(attacks.size()):
		var attack = attacks[i]
		var btn = Button.new()
		btn.text = attack
		btn.connect("pressed", self, "_on_AttackButton_pressed", [i])
		$Attacks.add_child(btn)
