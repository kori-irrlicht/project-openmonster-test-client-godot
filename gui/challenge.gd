extends Control


func _ready():
	network.connect("battle_invite_send", self, "_on_battle_invite_send")


func _on_ChallengeButton_pressed():
	var challengeID = int($"HBoxContainer/ChallengedID".text)
	network.send_challenge(challengeID)

func _on_battle_invite_send():
	$HBoxContainer/SuccessCheckbox.pressed = true
