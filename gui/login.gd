extends Control

onready var newNameField = $"Tabs/CreateAccount/VBoxContainer/HBoxContainer/NewName"
onready var newStarterField = $"Tabs/CreateAccount/VBoxContainer/HBoxContainer/NewStarter"
onready var accCreationFeedback = $"Tabs/CreateAccount/VBoxContainer/Message"

onready var loginIDField = $Tabs/Login/VBoxContainer/HBoxContainer/loginID

onready var tabs = $Tabs

func _ready():
	network.connect("account_created", self, "_on_AccountCreated")
	network.connect("login", self, "_on_login")
	network.connect("starter_received", self, "_on_StarterReceived")
	network.request_starter()

	$LocaleSelect.connect("locale_changed", self, "_retranslate")
	_retranslate()


func _retranslate():
	for i in range(tabs.get_tab_count()):
		var n = tabs.get_child(i).name
		tabs.set_tab_title(i, tr("gui."+n))


func _on_CreateAccountButton_pressed():
	var name = newNameField.text
	var species = newStarterField.get_selected_metadata()
	network.create_account(name, species)

func _on_AccountCreated(data):
	accCreationFeedback.text = "Your Breeder ID is " + str(data.breeder.id)

func _on_LoginButton_pressed():
	var breederID = int(loginIDField.text)
	network.login(breederID)

func _on_login(data):
	#get_tree().change_scene("res://scenes/battle/Challenge.tscn")
	#get_tree().change_scene("res://scenes/overworld/World.tscn")
	global.changeToWorld()

func _on_StarterReceived(data):
	newStarterField.clear()
	for i in range(len(data.species)):
		var spec = data.species[i]
		newStarterField.add_item(spec)
		newStarterField.set_item_metadata(i, spec)

func _():
	$Tabs.get_tab_title()
